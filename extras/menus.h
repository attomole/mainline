! menus.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
! Modificaciones: (C) Enrique D. Bosch "Presi"
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! 
! Rutinas minimas pero utiles de entrada/salida por menus
!
! Este m�dulo permite mostrar menus de opciones en pantalla para que
! el usuario elija una. Puede elegirla tecleando el n�mero de la
! opci�n, o pulsando sobre ella con el rat�n si su int�rprete admite
! hiperlinks.
!
! Incluye un gancho EventoGlk para detectar la pulsaci�n de un
! hyperlink y sustituirla por un comando textual como si el jugador
! hubiera escrito el n�mero de la opci�n
!
! Para usar este modulo, debes definir un objeto que actuar� como men�
! raiz, y del cual colgar�n objetos que son sus opciones. El titulo de
! cada opcion (y el del menu principal) es el nombre corto del
! mismo. Cuando el jugador elija una de esas opciones, se ejecutar� la
! propiead "rutina" del objeto correspondiente si este no ten�a hijos
! a su vez, o bien se presentar� un nuevo submen� en el que las
! opciones ser�n los hijos de la opci�n seleccionada
!
! La propiedad "rutina" a pesar de su nombre, puede ser tambien una
! cadena de texto, en cuyo caso ser� impresa cuando el jugador la
! seleccione.
!
! API:
!
! _menu_
!   clase del menu
! obj_menu
!   objeto predeterminado de la clase _menu_
! obj_menu.lanzar_menu(menu,borrar,nocero,nocont)
!   m�todo para ejecutar un men�, par�metros:
!     menu    : objeto men� ya creado para ser lanzado
!     borrar  : true para borrar la pantalla al lanzar el/los men�(s)
!     nocero  : true para que no aparezca la opci�n 0 en el/los men�(s)
!     nocont  : true para que el/los men�(s) solo se ejecute(n) una vez (no contin�e)
! obj_menu.mens_eleg
!   propiedad de texto del mensaje antes de elegir opci�n
! obj_menu.mens_fin
!   propiedad de texto de la opcion 0 del men�
! obj_menu.mens_noval
!   propiedad de texto de error cuando se da una opci�n no v�lida
! obj_menu.mens_cont
!   propiedad de texto del mensaje para continuar
! obj_menu.no_dos_puntos
!   propiedad true o false para imprimir ":" despu�s del texto obj_menu.mens_fin


#ifndef _MENUS_H_;
Message "[Modulo opcional de menus]";
System_file;
Constant _MENUS_H_;
Include "estilos";

Global Menu_en_Ejecucion = 0;
#ifndef array_input;
Array array_input ->TAMANYO_BUFFER_INPUT;
#endif;

Class _menu_
  with
    mens_eleg     "Elige opcion",
    mens_fin      "(0 finalizar)",
    mens_noval    "Opci�n no v�lida",
    mens_cont     "[Continuar]",
    no_dos_puntos false,

    ascii_a_entero
     [ cad len res i;

       if (len==0) return -1;
       res = 0;
       for (i=0:i<len:i++) 
       {
          res=res*10;
          if ((cad->i < '0') || (cad->i >'9')) return -1;
          res=res+(cad->i - '0');
       }
       return res;
     ],

    lanzar_menu
     [ raiz borrar nocero nocont
       i j k ultimoerror;

       ultimoerror="";
       while(1) {
           Menu_en_ejecucion = raiz;
           if (borrar) LimpiarVentana(gg_main);
           GlkEstiloCabecera();
           Output(raiz);
           GlkEstiloNormal();
           j=0; print "^";
           objectloop(i in raiz) {
               j++;
               GlkEstiloEnfatizado(); print "^  ",j, ": ";
               GlkEstiloNormal();
               GlkInicioLink(j);
               print (name) i;
               GlkFinLink();
           }
           GlkEstiloAlerta();
           print "^^",(string) ultimoerror;
           GlkEstiloNormal();
           ultimoerror="";
           print "^",(string) self.mens_eleg;
           if (~~nocero)
             {
               GlkInicioLink(100);
               print " ",(string) self.mens_fin;
               GlkFinLink();
             }
           if (~~self.no_dos_puntos) print " :";
           glk($0102, gg_main); ! Admitir eventos hyper
           BN_Input(gg_main, array_input);
           print "^";
           k=self.Ascii_A_Entero(array_input+WORDSIZE, array_input-->0);
           if ((k>j)||(k==-1)) {
               ultimoerror=self.mens_noval;
           }
           else if (k==0 && ~~nocero) return self.finalizar();
                else { 
                   j=0;
                   objectloop(i in raiz)  {
                   j++;
                   if (j==k) {
                       if (children(i)==0) {
                           if (i provides rutina);
                           Output(i, rutina);
                           if (nocero || nocont) return self.finalizar();
                           print "^", (string) self.mens_cont;
                           glk($0102, gg_main); ! Admitir eventos hyper
                           BN_Tecla(gg_main);
                       }
                       else
                       {
                         self.lanzar_menu(i,borrar,nocero,nocont);
                         if (nocero || nocont) return self.finalizar();
                       }
                   }
                }
           }
       }     
     ],
  private
    finalizar
     [;
         Menu_en_Ejecucion=0;
         glk($0103, gg_main); ! Cancelar eventos hyper
         return;
     ],
;

_menu_ obj_menu;


    ! Funciones antiguas, por compatibilidad hacia atr�s

[ AsciiAEntero cad len;
                           
  obj_menu.ascii_a_entero(cad,len);
];

[ LanzarMenu raiz borrar nocero nocont;

  obj_menu.lanzar_menu(raiz,borrar,nocero,nocont);
];    

    ! Gancho para capturar eventos de rat�n

_Gancho_ MenuEventoGlk EventoGlk
    with rutina [ evento como res;
             if (~~Menu_en_ejecucion) rfalse;
             if (evento-->0 == 8)
             {
                 if (como==1) return 2;
                 if (evento-->2 == 100) evento-->2 = 0;
                 res-->0=ImprimirEnMemoria(res+4, 3, datonumerico,
                                           evento-->2);
                 print evento-->2;
                 GlkEstiloNormal();
                 return 2;
             }
         ];
#endif;

