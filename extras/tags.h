! tags.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! Este modulo extra permite embeber tags en strings un poco al estilo
! HTML para cambiar los estilos de Glk. Las abreviaturas para cambio
! de estilo son:
!
! <e> enfatizado
! <p> preformateado (tipo courier)
! <c> cabecera
! <s> subcabecera
! <a> alerta
! <n> nota
! <b> bloque de texto
! <i> input
! <1> usuario1
! <2> usuario2
! </> normal
!
! Ejemplo de uso:
!
!   print "Hola, <a>esto es estilo alerta</>, vuelta a normal, y ahora
!      <e>enfatizado</>^";
!
!  para imprimir el caracter <, debes poner <<. Los dem�s caracteres
!  no tienen significado especial, salvo que vayan despues de un
!  <. Solo se tiene en cuenta el primer car�cter despues del < y los
!  dem�s se ignoran hasta que llegue el >, de modo que si lo prefieres
!  puedes poner:  print "Ten <enfasis>cuidado</enfasis>";
!  que no es m�s que otra forma m�s larga para poner:
!                 print "Ten <e>cuidado</>";
!
#ifndef _TAGS_H_;
Message "[Incluyendo <e>modulo tags</> para marcado tipo HTML de estilos]";
System_File;
Constant _TAGS_H_;

Include "estilos";
Include "filtros";

_Gancho_ FiltroTAGS FiltroDeSalida
    with estado 0,
    bloqueante BLOQUEAR_SI_TRUE,
    rutina [ c;
	! Esta rutina examina el chorro de caracteres de salida. Si
	! quiere dejar pasar un caracter al siguiente filtro, retorna
	! false. Si ella misma se ha ocupado ya del caracter, retorna
	! true.
    	if (c=='<') {
	    switch (self.estado) {
	     0: self.estado = 1; rtrue;
	     1:	self.estado=0;
	    	rfalse;  ! Dejamos que pase 
	     2: rtrue; ! No dejamos que pase
	    }
    	}
    	! Si no hemos encontrado un < hasta el momento, es texto normal
    	if (self.estado == 0) rfalse;
	
    	! Si hemos encontrado un <, viene un cambio de estilo
    	if (self.estado == 1) 
    	{
	    switch(c) 
	    {
	     '/': GlkEstiloNormal(); 
	     'e','E': GlkEstiloEnfatizado();
	     'p','P': GlkEstiloPreformateado();
	     'c','C': GlkEstiloCabecera();
	     's','S': GlkEstiloSubCabecera();
	     'a','A': GlkEstiloAlerta();
	     'n','N': GlkEstiloNota();
	     'b','B': GlkEstiloBloqueDeTexto();
	     'i','I': GlkEstiloInput();
	     '1': GlkEstiloUsuario1();
	     '2': GlkEstiloUsuario2();
	     default:
		if (self.estado==1) 
		{
		    glk(128, '<'); ! Imprimimos el < que habiamos
		    ! ignorado hasta ahora, y despues dejamos pasar el
		    ! caracter actual
	    	    self.estado=0;
	    	    rfalse;
		}
	    }
	    self.estado=2;
	    rtrue; 
    	}
    	if ((self.estado==2)&&(c=='>')) 
	    self.estado=0;
	rtrue;  ! No se imprime el > de cierre ni cualquier otra letra
	! que aparezca dentro de la pareja <>
    ];
#endif;    
