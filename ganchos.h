! ganchos.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
! ________________________________________________________________
!  Ejecutor de ganchos
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
!
#ifndef _GANCHOS_H_;
Message "..Incluyendo rutinas para manejo generico de ganchos";
System_file;
Constant _GANCHOS_H_;

! Posibles posiciones de un gancho
Constant GANCHO_EN_POSICION_POR_DEFECTO 0;
Constant GANCHO_EN_PRIMERA_POSICION 1;
Constant GANCHO_EN_ULTIMA_POSICION 2;
Constant GANCHO_EN_POSICION_DADA 3;
! La posici�n se especificar�a sumandosela a esta constante. Por
! ejemplo: (GANCHO_EN_POSICION_DADA + 4) es para ponerlo en 4�
! posici�n

! Tipos de bloqueo que puede causar un gancho en la cadena de ejecuci�n
Constant BLOQUEAR_NUNCA 0;
Constant BLOQUEAR_SI_TRUE 1;
Constant BLOQUEAR_SI_FALSE 2;
Constant BLOQUEAR_SIEMPRE 3;

! La clase gancho. El tenerlo como clase solo sirve para mejorar la
! legibilidad del fuente y para tener unos valores por defecto en sus
! propiedades (pues no son propiedades comunes)
Class _Gancho_
 with	rutina [; ],
	bloqueante BLOQUEAR_NUNCA,
	posicion GANCHO_EN_POSICION_POR_DEFECTO;

! Este bucle recorre todos los ganchos del juego y reordena los que
! tengan algo distinto de cero en su campo "posicion"
[ InicializarGanchos i;
    objectloop(i ofclass _Gancho_)
	if (parent(i)) {
	    if (i.posicion) MoverGancho(i, parent(i));
	}
];

! Esta rutina mueve el gancho seg�n lo que se indique en su campo
! "posicion"
[ MoverGancho g padre;
    switch(g.posicion) 
    {
     GANCHO_EN_POSICION_POR_DEFECTO: return;
     GANCHO_EN_PRIMERA_POSICION:
	move g to padre;
     GANCHO_EN_ULTIMA_POSICION:
	MoverComoHijoUltimo(g, padre);
     default:
	if (g.posicion >=GANCHO_EN_POSICION_DADA)
	    MoverComoHijoN(g, padre, g.posicion-GANCHO_EN_POSICION_DADA);
    }
];

! La siguiente rutina recibe un "gancho-padre" y una lista de
! argumentos variable. Lo que hace es recorrer todos ganchos hijos de
! este gancho padre y para cada uno de ellos ejecuta su "rutina"
! pas�ndole todos los argumentos recibidos.
!
! Si alcanza un gancho con la propiedad bloqueante, interrumpe la
! secuencia de ejecuciones (no eval�a los restantes ganchos-hijo)
! seg�n el valor retornado por el gancho bloquante.
!
! Si no hay bloqueos, se retorna finalmente el valor retornado por el
! �ltimo gancho-hijo ejecutado que haya retornado algo distinto de
! false (o cero).
!
[ EjecutarGancho _vararg_count g i x r;
    @copy sp g;
    _vararg_count--;
    ! Si no hay tal gancho-padre retornamos
    if (g==0) return;
    ! Si no es de la clase gancho retornamos
    if (~~(g ofclass _Gancho_))
	"^[** Error de programaci�n: Se ha llamado EjecutarGancho
	sobre	", (name) g, ", que no es un gancho **]";
    ! Recorremos los hijos de g
    r=false;
    objectloop (i in g) 
    {
	! Y ejecutamos los que sean de la clase gancho
	if (i ofclass _Gancho_) 
	{
	    ! Vamos a llamar a i.rutina pas�ndole todos los par�metros
	    ! que hemos recibido en esta llamada a EjecutarGancho
!	    print "Voy a ejecutar el gancho ", (name) i, " con ",
!		_vararg_count, " par�metros^";
	    @stkcopy _vararg_count;
	    @copy rutina sp;
	    _vararg_count++;
	    @copy i sp;
	    _vararg_count++;
	    @call CA__Pr _vararg_count x;
!	    print "Ha retornado el gancho ", (name) i, " con el
!		resultado ", x, "^";
	    
	    ! Y ahora comprobemos si la cadena de ejecuciones debe
	    ! interrumpirse:
	    if (i.bloqueante==BLOQUEAR_SIEMPRE) return (x);
	    if ((i.bloqueante==BLOQUEAR_SI_TRUE)&& x) return (x);
	    if ((i.bloqueante==BLOQUEAR_SI_FALSE)&& ~~x) return (x);
	    _vararg_count=_vararg_count-2;
	    if (x) r=x;
	}
    }
    return r;
];
#endif;
