! glk.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
!________________________________________________________________
! Inicializa las ventanas Glk. El gancho por defecto aqu� suministrado
! realiza una inicializaci�n m�nima (s�lo una ventana)
!
! ________________________________________________________________
! Cambios con respecto a la versi�n 080601
!   A�adido el gancho ReinicializarGlk, necesario para poder restaurar
!   convenientemente los objetos Glk tras un LOAD o RESTART. (Aqui no
!   se implementa ese gancho, pero cualquier librer�a de desarrollo de
!   juegos deber� hacerlo, vease pawsmole como ejemplo).
!
#ifndef _GLK_H_;
Message "[Incluyendo inicializacion de Glk minima]";
System_file;
Constant _GLK_H_;

Global gg_main;

_Gancho_ InicializarGlk;
_Gancho_ ReinicializarGlk;

_Gancho_ GlkMinimo InicializarGlk
    with rutina [;
	     if (gg_main == 0)
    	     	 gg_main = glk(35, 0, 0, 0, 3, 201);
    	     glk(47, gg_main);
	     LimpiarVentana(gg_main);
	 ];

_Gancho_ ModuloGlk InicializarModulos
    with rutina [;
    	     @setiosys 2 0;
	     EjecutarGancho(ReinicializarGlk);
	     EjecutarGancho(InicializarGlk);
	 ];
#endif;
