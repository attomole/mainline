! estilos.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
! ________________________________________________________________
! Estilos de Glk
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
#ifndef _ESTILOS_H_;
System_file;
Constant _ESTILOS_H_;

[ GlkEstiloNormal ;    glk($86,0);];
[ GlkEstiloEnfatizado ;    glk($86,1);];
[ GlkEstiloPreformateado ;    glk($86,2);];
[ GlkEstiloCabecera;    glk($86,3);];
[ GlkEstiloSubCabecera;    glk($86,4);];
[ GlkEstiloAlerta;    glk($86,5);];
[ GlkEstiloNota;    glk($86,6);];
[ GlkEstiloBloqueDeTexto;    glk($86,7);];
[ GlkEstiloInput;    glk($86,8);];
[ GlkEstiloUsuario1;    glk($86,9);];
[ GlkEstiloUsuario2;    glk($86,10);];
[ GlkInicioLink n; glk($0100, n);];
[ GlkFinLink; glk($0100, 0);];
#endif;
