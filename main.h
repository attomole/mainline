! main.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! Proporciona la rutina de arranque main() que prepara el mecanismo de
! ganchos, inicializa los m�dulos y llama a Principal(), funci�n a
! suministrar por el programador
!
#ifndef _MAIN_H_;
System_file;
Constant _MAIN_H_;
Message "[Incluyendo rutina main minima]";
[ main ;
    InicializarGanchos();
    EjecutarGancho(InicializarModulos);
    Principal();
];
#endif;

