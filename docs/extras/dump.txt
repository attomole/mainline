dump.h
================================================================
v0.6.1
Autor:  Zak McKracken
________________________________________________________________
Sinopsis 

   Constantes:
     Ninguna.

   Rutinas:
     MostrarHijos
     MostrarArbol
     MostrarObjeto
     MostrarTipo
     MostrarPropiedades
     MostrarOrdenacionGancho
     MosrrarTodosLosGanchos

   Ganchos:
      Ninguno.

   Variables globales:
      Ninguna.

   Incluye: 
      Nada. (pero requiere glk inicializado)

________________________________________________________________
Funcionalidad

  Se proporcionan varias rutinas para volcado por pantalla de
  informaci�n interna de los objetos y del sistema de ganchos de
  attoMOLE. La informaci�n que muestran algunas de las rutinas es
  altamente t�cnica. Se recomienda tener a mano la especificaci�n
  t�cnica de �nform-Glulx para interpretar esta informaci�n. Estas
  rutinas son muy poco �tiles para el p�blico en general, y su
  principal utilidad estrib� en la ayuda al desarrollo del propio
  attoMOLE.

  No obstante, MostrarOrdenacionGancho y MostrarTodosLosGanchos
  proporcionan informaci�n de m�s alto nivel y utilidad.

________________________________________________________________
Rutinas

 * MostrarHijos(objeto)
     Muestra la lista de hijos del "objeto", simplemente sus
     nombres,uno en cada l�nea. No muestra recursivamente los
     descendientes de sus hijos

 * MostrarArbol(objeto)
     Muestra la lista de hijos de "objeto" descendiendo recursivamente
     por sus hijos. Muestra el nombre del objeto, uno en cada l�nea,
     indentado hacia la derecha para indicar dentro de qu� objeto se
     halla.

 * MostrarObjeto(objeto)
     Muestra la estructura interna del objeto, tal y como es
     almacenado en la m�quina Glulx. De poco inter�s real.

 * MostrarTipo(pdato)
     Interpreta pdato como un puntero a un dato, e imprime en pantalla
     una cadena que indica el tipo del dato en cuesti�n.

 * MostrarPropiedades(objeto)
     Muestra cu�ntas propiedades tiene el objeto, y seguidamente
     informaci�n espec�fica sobre cada propiedad: su n�mero
     identificador (y su nombre fuente) su longitud (n�mero de datos
     que contiene), la direcci�n de memoria en la que son almacenados
     estos datos, y los flags (si es propiedad privada o p�blica).

     De poco inter�s real.

 * MostrarOrdenacionGancho(gancho, pref)
     Muestra en qu� orden ser�n ejecutadas las rutinas que han sido
     instaladas para ese gancho (o la cadena "gancho vac�o" si no ha
     sido instalada ninguna).
 
     El par�metro opcional "pref" es un prefijo que se emitir� delante
     de cada l�nea, antes del nombre de cada gancho.

 * MostrarTodosLosGanchos()
     Muestra todos los ganchos declarados, y para cada uno de ellos el
     orden de ejecuci�n de sus rutinas instaladas.



