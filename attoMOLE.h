! attoMOLE.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
#ifndef TARGET_GLULX;
Message fatalerror "Lo siento attoMOLE solo compila para glulx. Intenta poniendo -G al compilar.";
#endif;
#ifndef _ATTOMOLE_H_;
Message "Incluyendo attoMOLE. (c) 2001 Zak";
System_file;
Constant _ATTOMOLE_H_;
#ifdef DEBUG;
Include "debug";
#endif;
Include "nag.h";
Include "ganchos.h";
Include "io.h";
Include "token.h";
Include "modulos.h";
#endif;
