! token.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! Tokenizador gen�rico programable
!
#ifndef _TOKEN_H_;
Message "..Incluyendo tokenizador";
System_file;
Constant _TOKEN_H_;
Constant MAX_PALABRAS_BUFFER = 20;
Constant TAMANYO_BUFFER_PALABRAS = 84;        ! 4 + MAX_PALABRAS_BUFFER*4;
Constant TAMANYO_BUFFER_INDICES = 164;        ! 4 + MAX_PALABRAS_BUFFER*8

Array gg_tokenbuf -> DICT_WORD_SIZE;
Array SeparadoresTokens string ' ' '.' ',' '"' 0 0 0 0 0 0 0 0 0 0 0 0 0;
Global num_separadores = 4;

! Funcion de tokenizado, el resultado se separa en dos arrays. Uno
! que tiene las palabras de diccionario y otro que tiene punteros al
! buffer de letras indicando d�nde comienza cada palabra y su longitud
[ Tokenizar buf palabras indices 
    cx numwords len bx ix wx wpos wlen val res dictlen entrylen;
    len = buf-->0;
    buf = buf+WORDSIZE;
    
    ! Primero, partir el buffer en palabras, usando para ello la lista
    ! de separadores
    cx = 0;
    numwords = 0;
    while (cx < len) {
    	while (cx < len && buf->cx == ' ')
      	    cx++;
    	if (cx >= len)
      	    break;
    	bx = cx;
    	if (EstaEnArrayBytes(buf->cx, SeparadoresTokens+1, num_separadores)) {
      	    cx++;
	}
	else {
      	    while (cx < len && ~~EstaEnArrayBytes(buf->cx, SeparadoresTokens+1, num_separadores))
		cx++;
    	}
    	indices-->(numwords*2+2) = (cx-bx);
    	indices-->(numwords*2+1) = WORDSIZE+bx;
    	numwords++;
    	if (numwords >= MAX_PALABRAS_BUFFER)
      	    break;
    }
    palabras-->0 = numwords;  
    indices-->0 = numwords;
    
    ! Ahora buscamos cada palabra en el diccionario
    dictlen = #dictionary_table-->0;
    entrylen = DICT_WORD_SIZE + 7;
    for (wx=0 : wx < numwords : wx++) {
       wlen = indices-->(wx*2+2);
       wpos = indices-->(wx*2+1);
       ! Copiamos la palabra en el array gg_tokenbuf, recortandola al
       ! numero de caracteres expresado en DICT_WORD_SIZE, y pasandola
       ! a minusculas
       if (wlen > DICT_WORD_SIZE)
         wlen = DICT_WORD_SIZE;
       cx = wpos-WORDSIZE;
       for (ix=0 : ix < wlen : ix++) {
          gg_tokenbuf->ix = glk($00A0, buf->(cx+ix)); ! char_to_lower
       }
       for ( : ix < DICT_WORD_SIZE : ix++) {
         gg_tokenbuf->ix = 0;
       }
       val = #dictionary_table + WORDSIZE;
       @binarysearch gg_tokenbuf DICT_WORD_SIZE val entrylen dictlen
         1 1 res;
       palabras-->(wx+1) = res;
    }
];

[ AnyadirSeparador x;
    if (num_separadores == SeparadoresTokens->0) return -1;
    if (Unsigned__Compare(x, 255) >=1) rfalse;
    num_separadores++;
    SeparadoresTokens->num_separadores = x;
    rtrue;
];


#endif;
