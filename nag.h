! nag.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! Nivel de Abstraccion GLulx
!
!  Implementa todas las rutinas del "veneer", es decir, las rutinas
!  que son ejecutadas ante ciertos comandos Inform. En estas rutinas
!  es donde realmente se define la sem�ntica de operaciones como give,
!  provides, has, ::, ->, etc...
!
!  Usamos este nivel no para redefinir la sem�ntica de esos
!  operadores, sino para capturar en tiempo de ejecuci�n cuando se
!  hace uso de alguno de ellos, con vistas a detecci�n de errores y
!  trazado (depuraci�n)
! ________________________________________________________________
!  Cambios con respecto a la version 080601
!    * Corregidas las rutinas que imprimen "el", "un" que antes
!      mostraban el numero del objeto en lugar de su nombre
!
#ifndef _NAG_H_;
Message "..Incluyendo el Nivel de Abstraccion Glulx";
System_file;
Constant _NAG_H_;

! Llamada ante el comando box
!
[ Box__Routine
    maxwid arr ix;
    maxwid = 0;
    glk($0086, 7);  ! Estilo BloqueDeTexto
    for (ix=0 : ix<arr-->0 : ix++) {
	print (string) arr-->(ix+1);
	new_line;
    }
    glk($0086, 0);  ! Estilo Normal
];

! Ejecutado desde el comando <accion uno otro>, o <<accion uno otro>>
! Esta rutina por defecto realmente no hace nada, no llega a ejecutar
! la acci�n. POR HACER.
![ R_Process
!    a b c d; print "Action <", a, " ", b, " ", c, " ", d, ">^";
!];     
! Ejecutado ante print (the) obj
! Esta version minima no tiene en cuenta genero ni numero. POR HACER
[ DefArt
    obj; print "el ", (name) obj;
];
! Ejecutado ante print (a) obj
! Esta version minima no tiene en cuenta genero ni numero. POR HACER
[ InDefArt
    obj; print "un ", (name) obj;
];
! Ejecutado ante print (The) obj
! Esta version minima no tiene en cuenta genero ni numero. POR HACER
[ CDefArt
    obj; print "El ", (name) obj;
];
! Ejecutado ante print (name) obj
[ PrintShortName
    obj q;
    switch(MetaTipo(obj))
    {
     TIPO_ARRAY: print "(array)";
     TIPO_STRING: print "(string en ", obj, ")";
     TIPO_DICCIONARIO: print "(palabra de diccionario en ", obj, ")";
     TIPO_RUTINA: print "(rutina en ", obj, ")";
     TIPO_OBJETO:
      	q = obj-->3; @streamstr q;
     TIPO_CLASE:
	print "Class "; q = obj-->3; @streamstr q;
     default:
	print "(nada)";
    }
];
! Ejecutado ante print (number) obj
! Esta rutina minima en realidad deber�a llamar a otra que imprima el
! n�mero en castellano.
[ EnglishNumber
    obj; print obj;
];     
! Ejecutado ante print (property) obj
[ Print__PName
    prop ptab cla maxcom minind maxind str;
    ! Si la propiedad tiene bits en la parte alta, ha sido heredada de
    ! una clase, entonces prefijamos el nombre de la propiedad con el
    ! nombre de la clase
    if (prop & $FFFF0000) {
	cla = #classes_table-->(prop & $FFFF);
	print (name) cla, "::";
	@ushiftr prop 16 prop;
    }
    ptab = #identifiers_table;
    maxcom = ptab-->1;         ! N�mero de propiedades comunes
    minind = INDIV_PROP_START; ! Numero de la menor propiedad individual
    maxind = minind + ptab-->3;! N�mero de la mayor propiedad individual
    str = 0;
    if (prop >= 0 && prop < maxcom) { ! Es una propiedad comun conocida
	str = (ptab-->0)-->prop;
    }
    else if (prop >= minind && prop < maxind) {
	! Es una propiedad individual conocida
	str = (ptab-->2)-->(prop-minind);
    }
    if (str)
	print (string) str;
    else
	! Es una propiedad desconocida
	print "<n�mero ", prop, ">";
];     

! Llamada cuando se intenta asignar un valor a una propiedad de un
! objeto, como p.e. obj.dato = 3;
!
[ WV__Pr
    obj id val addr;
    addr = obj.&id;
    if (addr == 0) { ! El objeto no tiene esa propiedad
	RT__Err("escribir", obj, id);
	return 0;
    }
    addr-->0 = val; ! Se asigna
    return 0;
];     

! Llamada cuando se intenta leer el valor de la propiedad de un
! objeto, como p.e. print obj.dato;
[ RV__Pr
    obj id addr;
    addr = obj.&id;
    if (addr == 0) { ! No tiene esta propiedad, pero puede deberse a
	! que sea una propiedad comun. Las propiedades comunes pueden
	! leerse incluso si el objeto no las ha declarado. En este
	! caso se lee su valor por defecto
	if (id > 0 && id < #identifiers_table-->1) 
	{
	    !  Valor por defecto para esta propiedad comun
	    return #cpv__start-->id; 
	}
	RT__Err("leer", obj, id);
	return 0;
    }
    return addr-->0;
];     
! Esta es llamada cuando se intenta ejecutar una propiedad de una
! rutina mediante por ejempo obj.antes(); Aqui se implementan tambi�n
! los casos particulares en los que el rutina en cuestion sea una de
! las que Inform predefine para las clases. (create, recreate,
! destroy, remaining y copy). O sea que estas propiedades no se
! heredan realmente, sino que se tratan de forma particular.
[ CA__Pr
    _vararg_count obj id zr s s2 z addr len m val;
    @copy sp obj;  ! Objeto
    @copy sp id;   ! propiedad a ejecutar
    _vararg_count = _vararg_count - 2;
    zr = Z__Region(obj); ! Averiguamos el tipo del objeto
    if (zr == 2) { ! Si es una rutina
	if (id == call) { ! tan solo admite una propiedad, que es call
	    ! Se inicializa la variable sender con el objeto desde el
	    ! que se est� llamando, y la variable self con el objeto
	    ! llamado.
	    s = sender; sender = self; self = obj;
!	    #ifdef action; sw__var=action; #endif;
	    ! Y se hace la llamada
	    @call obj _vararg_count z;  ! z recoge el resultado
	    ! Se restauran self y sender.
	    self = sender; sender = s;
	    return z;
	}
	jump Call__Error;
	! Se ha intentado llamar a una propiedad distinta de call para
	! un objeto rutina
    }
    if (zr == 3) {
	! Tipo string, admite dos propiedades miembro:
	! print y print_to_array
	if (id == print) {
	    @streamstr obj; rtrue;
	}
	if (id == print_to_array) {
	    ! Comprobar que recibimos suficientes par�metros
	    if (_vararg_count >= 2) {
		@copy sp m;   ! pop m (direcci�n del array)
		@copy sp len; ! pop len (numero de bytes a escribir)
	    }
	    else {
		@copy sp m;   ! pop m (direcci�n del array)
		len = $7FFFFFFF; ! len = maximo entero posible
	    }
	    ! Codigo de print_to_array
	    s2 = glk($0048); ! glk_stream_get_current
	    s = glk($0043, m+4, len-4, 1, 0); ! glk_stream_open_memory
	    if (s) {
		glk($0047, s);  ! glk_stream_set_current
		@streamstr obj; ! Imprimir el objeto
		glk($0047, s2); ! glk_stream_set_current (el original)
		@copy $ffffffff sp; ! push -1
		@copy s sp;         ! push s (el stream de memoria)
		@glk $0044 2 0;     ! glk_stream_close (cerrarlo)
		@copy sp len;       ! pop len
		@copy sp 0;         ! pop 0
		m-->0 = len;        ! asignar longitud
		return len;         ! retornar esa longitud
	    }
	    rfalse; ! Si no se ha podido crear el stream en memoria
	}
	jump Call__Error;
	! Si se ha intentado llamar a una propiedad distinta de print
	! o print_to_array sobre un objeto de tipo string.
    }
    if (zr ~= 1) ! Si no es un objeto...
	jump Call__Error; ! No se puede llamar a sus propiedades
    #ifdef _DEBUG_H_;
    ! Trazar la llamada
    if (bits_debug & 1 ~= 0) {
	bits_debug--; ! No trazar las llamadas a propiedades ....
	print "[ ~", (name) obj, "~.", (property) id, "(";
	@stkcopy _vararg_count;
	for (val=0 : val < _vararg_count : val++) {
	    if (val) print ", ";
	    @streamnum sp;
	}
	print ") ];^";
	bits_debug++; !... hasta aqui
    }
    #endif;
    if (obj in Class) {
	! Si es una clase, se puede llamar a algunas
	! propiedades miembro "expeciales"
	switch (id) {
	 remaining:
	    return Cl__Ms(obj, id);
	 copy:
	    @copy sp m;
	    @copy sp val;
	    return Cl__Ms(obj, id, m, val);
	 create, destroy, recreate:
	    m = _vararg_count+2;
	    @copy id sp;
	    @copy obj sp;
	    @call Cl__Ms m val;
	    return val;
	}
    }
    ! LLegados a este punto, se trata de un objeto normal. Vamos a
    ! llamar a su propiedad
    addr = obj.&id;
    if (addr == 0) {
	! No tiene esta propiedad, pero tal vez sea una propiedad
	! comun y se pueda evaluar su rutina por defecto
	if (id > 0 && id < INDIV_PROP_START) {
	    addr = #cpv__start + 4*id;
	    len = 4;
	}
	else {
	    jump Call__Error;
	}
    }
    else {
	len = obj.#id;
    }
    for (m=0 : 4*m<len : m++) {
	val = addr-->m;
	if (val == -1) rfalse;
	switch (Z__Region(val)) {
	 2:
	    s = sender; sender = self; self = obj; s2 = sw__var;
!	    #ifdef LibSerial;
!	    if (id==life) sw__var=reason_code; else sw__var=action;
!	    #endif;
	    @stkcopy _vararg_count;
	    @call val _vararg_count z;
	    self = sender; sender = s; sw__var = s2;
	    if (z ~= 0) return z;
	 3:
	    @streamstr val;
	    new_line;
	    rtrue;
	 default:
	    return val;
	}
    }
    rfalse;
    .Call__Error;
    RT__Err("enviar mensaje", obj, id);
    rfalse;
];     

! Esta rutina implementa el operador de pre-incremento de una
! propiedad, es decir: ++obj.prop
[ IB__Pr
    obj identifier x;
    x = obj.&identifier;
    ! No existe, no se puede incrementar
    if (x==0) { RT__Err("incrementar", obj, identifier); return; }
!    #ifdef INFIX;
!    if (obj has infix__watching || (bits_debug & 15)) RT__TrPS(obj,identifier,(x-->0)+1);
!    #ifnot; #ifdef DEBUG;
    ! trazar
    #ifdef _DEBUG_H_;
    if (bits_debug & 2) RT__TrPS(obj,identifier,(x-->0)+1);
    #endif; 
    return ++(x-->0);
];     

! Esta implementa el operador de post-incremento de propiedades: obj.prop++
[ IA__Pr
    obj identifier x;
    x = obj.&identifier;
    if (x==0) { RT__Err("incrementar", obj, identifier); return; }
!    #ifdef INFIX;
!    if (obj has infix__watching || (bits_debug & 15))
!	RT__TrPS(obj,identifier,(x-->0)+1);
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 2) RT__TrPS(obj,identifier,(x-->0)+1);
    #endif;
    return (x-->0)++;
];     
! Predecremento de propiedad --obj.prop
[ DB__Pr 
    obj identifier x;
    x = obj.&identifier;
    if (x==0) { RT__Err("decrementar", obj, identifier); return; }
!    #ifdef INFIX;
!    if (obj has infix__watching || (bits_debug & 15)) RT__TrPS(obj,identifier,(x-->0)-1);
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 2) RT__TrPS(obj,identifier,(x-->0)-1);
    #endif; 
    return --(x-->0);
];     

! Postdecremento de propiedad: obj.pro--
[ DA__Pr
    obj identifier x;
    x = obj.&identifier;
    if (x==0) { RT__Err("decrementar", obj, identifier); return; }
!    #ifdef INFIX;
!    if (obj has infix__watching || (bits_debug & 15)) RT__TrPS(obj,identifier,(x-->0)-1);
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 2) RT__TrPS(obj,identifier,(x-->0)-1);
    #endif;
    return (x-->0)--;
];

! Implementa el operador & en objeto.&propiedad, y retorna la
! direcci�n donde est�n almacenados los datos de esa propiedad, o
! bien 0 si el objeto no proporciona esa propiadad. Ver tambi�n
! RL__Pr()
[ RA__Pr
    obj id cla prop ix;
    if (id & $FFFF0000) { ! Si es una propiedad de una superclase
	cla = #classes_table-->(id & $FFFF);
	if (~~(obj ofclass cla)) return 0;
	@ushiftr id 16 id;
	obj = cla;
    }
    prop = CP__Tab(obj, id); ! Obtenemos aqui la direcci�n de la
    ! propiedad, busc�ndola en la tabla de propiedades del objeto
    ! obj. Lo que obtenemos es la direcci�n dentro de la tabla de
    ! propiedades del objeto donde se almacenan las caracter�sticas de
    ! esta propiedad (su tama�o, la direcci�n donde est�n sus datos,
    ! etc). La rutina retorna 0 si no encuentra esa propiedad en la
    ! lista.
    if (prop==0) return 0;
    if (obj in Class && cla == 0) {
	if (id < INDIV_PROP_START || id >= INDIV_PROP_START+8)
	    return 0;
	! Si estamos tratando de acceder a las "seudopropiedades"
	! create, recreate, etc.. se retorna 0 en este caso
    }
    if (self ~= obj) {
	@aloadbit prop 72 ix;
	if (ix) return 0;
	! Si estamos accediendo a una propiedad de otro objeto,
	! comprobemos antes si es privada. Si lo es, no se puede
	! averiguar su direcci�n (retornar 0)
    }
    ! Si pasa los test, retornamos la direcci�n donde est�n los datos
    ! asociados a esa propiedad (es uno de los campos de la estructura
    ! a que apunta prop)
    return prop-->1;
];     

! Implementaci�n del operador # en objeto.#propiedad para averiguar la
! longitud de una propiedad. Tiene en cuenta las mismas limitaciones
! que la rutina anterior
[ RL__Pr
    obj id cla prop ix;
    if (id & $FFFF0000) { ! Si es un operador de superclase
	cla = #classes_table-->(id & $FFFF);
	if (~~(obj ofclass cla)) return 0;
	@ushiftr id 16 id;
	obj = cla;
    }
    prop = CP__Tab(obj, id);
    if (prop==0) return 0;
    ! Si no se encuetra esta propiedad en el objeto
    
    if (obj in Class && cla == 0) {
	if (id < INDIV_PROP_START || id >= INDIV_PROP_START+8)
	    return 0;
	! Si es una de las "pseudo propiedades" create, destroy...
    }
    if (self ~= obj) {
	! Si es una propiedad de otro objeto
	@aloadbit prop 72 ix;
	! Se comprueba si es privada o publica
	if (ix) return 0;
	! y se retorna 0 si es privada
    }
    @aloads prop 1 ix;
    ! Obtenemos el n�mero de elementos almacenados en esta propiedad y
    ! lo retornamos multiplicado por 4 (pues debemos retornar el
    ! n�mero de bytes)
    return WORDSIZE * ix;
];     

! Implementaci�n del operador de superclase ::, que devuelve un
! identificador de propiedad compuesto, que es un n�mero de 32 bits
! que lleva en la parte alta el identificador de clase y en la baja el
! de la propiedad.
[ RA__Sc
    cla id j;
    if ((cla notin Class) && (cla ~= Class or String or Routine or Object)) {
	RT__Err("aplicarle un '::' de superclase", cla, -1);
	rfalse;
    }
    ! Buscamos si cla es realmente un identificador de clase
    for (j=0 : #classes_table-->j ~= 0 : j++) {
	if (cla == #classes_table-->j) {
	    return (id * $10000 + j);
	}
    }
    RT__Err("utilizar", cla, id);
    rfalse;
];     

! Implementaci�n de "provides"
[ OP__Pr
    obj id zr;
    zr = Z__Region(obj);
    if (zr == 3) { ! Si el objeto es un string
	if (id == print or print_to_array) rtrue;
	rfalse;
	! Solo puede tener dos propiedades: print o print_to_array
    }
    if (zr == 2) { ! Si es una rutina
	if (id == call) rtrue;
	rfalse;
	! Solo puede tener una propiedad: call
    }
    if (zr ~= 1) rfalse;
    ! Si es otra cosa (no-objeto) no tiene la propiedad
    
    if (id >= INDIV_PROP_START && id < INDIV_PROP_START+8) {
	if (obj in Class) rtrue;
	! Si es una de las propiedades create, recreate, etc.. y el
	! objeto es una clase, s� tiene la propiedad
    }
    if (obj.&id ~= 0) ! Si la direcci�n de datos de su propiedad es
	! distinta de cero, es que la tiene (esto es evaluado por
	! RA__Pr)
	rtrue;
    rfalse; ! En otro caso, no tiene la propiedad
];

! Implementaci�n del operador ofclass
[ OC__Cl
    obj cla zr jx inlist inlistlen;
    zr = Z__Region(obj);
    if (zr == 3) { !Si el objeto es una cadena
	if (cla == String) rtrue;
	rfalse;
	! Solo ser� cierto si la clase es String
    }
    if (zr == 2) { ! Si el objeto es una rutina
	if (cla == Routine) rtrue;
	rfalse;
	! S�lo ser� cierto si la clase es Routine
    }
    if (zr ~= 1) rfalse;
    ! Si el objeto no es un objeto, es falso que sea de cualquier
    ! clase.
    
    if (cla == Class) { ! Test de si el objeto es ofclass Class
	if (obj in Class
	    || obj == Class or String or Routine or Object)
	    rtrue;
	! Son de la metaclase Class las clases, y las cuatro
	! metaclases 
	rfalse;
    }
    if (cla == Object) { ! Test de si es de la clase Object
	if (obj in Class
	    || obj == Class or String or Routine or Object)
	    rfalse;
	! Son de la clase Object todos los objetos del juego, excepto
	! las clases y las propias cuatro metaclases
	rtrue;
    }
    if (cla == String or Routine) rfalse;
    ! Un objeto no es nunca de la clase String o Routine
    
    if (cla notin Class) {
	RT__Err("aplicarle 'ofclass'", cla, -1);
	rfalse;
	! El segundo operando no era una clase
    }
    inlist = obj.&2;
    ! La segunda propiedad de un objeto es una lista de a qu� clases
    ! pertenece. Buscamos en esa lista si est� ah� la clase por la que
    ! se nos pregunta.
    if (inlist == 0) rfalse;
    inlistlen = (obj.#2) / WORDSIZE;
    for (jx=0 : jx<inlistlen : jx++) {
	if (inlist-->jx == cla) rtrue;
    }
    rfalse;
];     

! Primitiva que copia dos objetos (en realidad, copia sus
! propiedades). El objeto destino debe tener las mismas propiedades y
! de la misma longitud que el objeto fuente. Las propiedades que no
! tiene o no son iguales no ser�n copiadas.
[ Copy__Primitive
    o1 o2 p1 p2 pcount i j propid proplen val pa1 pa2;

    ! Primero, copiar sus atributos 
    for (i=1 : i<=NUM_ATTR_BYTES : i++) {
	o1->i = o2->i;
    }
    ! Ahora, copiar los restantes datos internos del objeto
    p2 = o2-->((NUM_ATTR_BYTES+1)/4+2);  ! Su tabla de propiedades
    pcount = p2-->0;  ! �CU�ntas propiedades tiene?
    p2 = p2+4; ! p2 apunta a la estructura "propiedad"
    for (i=0 : i<pcount : i++) {
	@aloads p2 0 propid;  ! Leer el identificador
	@aloads p2 1 proplen; ! Leer la longitud
	p1 = CP__Tab(o1, propid); ! �Tiene el objeto destino esa propiedad?
	if (p1) { ! Si la tiene
	    @aloads p1 1 val; ! Veamos cu�l era su longitud
	    if (proplen == val) { ! Si era la misma, simplemente
		! copiamos una lista en otra. Si no era la misma no se
		! puede hacer la copia del objeto
		@aloads p2 4 val;
		@astores p1 4 val;
		pa1 = p1-->1; ! Direcci�n del array destino
		pa2 = p2-->1; ! Direcci�n del array fuente
		for (j=0 : j<proplen : j++)
                    pa1-->j = pa2-->j;
	    }
	}
	p2 = p2+10;
    }
];     

! Rutina para imprimir los errores en tiempo de ejecuci�n, detectados
! por el veneer.
!
[ RT__Err
    crime obj id size p q;
    print "^[** Error de programaci�n: ";
    if (crime<0) jump RErr;
    if (crime==1) { print "class "; q = obj-->3; @streamstr q;
	": 'create' s�lo puede tener 0 a 3 par�metros **];";}
    if (crime == 40) "se ha intentado cambiar la variable de impresi�n ",
	obj, "; debe estar entre 0 y ", #dynam_string_table-->0-1, " **];";
    if (crime == 32) "objectloop roto porque el objeto ",
         (name) obj, " fue movido mientras el bucle pasaba sobre �l **];";
    if (crime == 33) "se intent� print (char) ", obj,
	", el cual no es un car�cter v�lido de salida para Glk **];";
    if (crime == 34) "se intent� print (address) sobre algo que no es
    una direcci�n de palabra de diccionario **];";
    if (crime == 35) "se intent� print (string) sobre algo que no es
    un string **];";
    if (crime == 36) "se intent� print (object) sobre algo que no es
    un objeto o una clase **];";
    if (crime < 32) { print "se intent� ";
	if (crime >= 28) {
	    if (crime==28 or 29) print "leer de ";
	    else print "escribir en ";
            if (crime==29 or 31) print "-";
	    print "->", obj, " en ";
	    if(size>=8) { print" (->)"; size=size-8; }
            if(size>=4) { print" (-->)"; size=size-4; }
            switch(size){
	     0,1:q=0;
	     2:print " el string"; q=1;
	     3:print " la tabla";q=1;
	    }
	    " array ~", (string) #array_names_offset-->(p+1),
		"~, que s�lo tiene elementos de ", q,
		" a ",id," **];";
	}
	if (crime >= 24 && crime <=27) {
	    if (crime<=25) print "leer";
	    else print "escribir";
	    print " fuera de la memoria usando ";
            switch(crime) {
	     24,26:"-> **];";
	     25,27:"--> **];"; }
	}
	if (crime < 4) print "testear ";
	else
            if (crime < 12 || crime > 20)
		print "encontrar el ";
	    else
         	!if (crime < 14)
		print "usar ";
	if (crime==20) "dividir por cero **];";
	print "~";
	switch(crime) {
         2: print "in~ o ~notin"; 3: print "has~ o ~hasnt";
         4: print "parent"; 5: print "eldest"; 6: print "child";
         7: print "younger"; 8: print "sibling"; 9: print "children";
         10: print "youngest"; 11: print "elder";
         12: print "objectloop"; 13: print "}~ al final de ~objectloop";
         14: "give~ sobre ", (name) obj, " **];";
         15: "remove~ ", (name) obj, " **];";
         16,17,18: print "move~ ", (name) obj, " to ", (name) id;
            if (crime==18) { print ", lo que causar�a un bucle: ",(name) obj;
         	p=id; if (p==obj) p=obj;
         	else do { print " en ", (name) p; p=parent(p);} until (p==obj);
         	" en ", (name) p, " **];"; }
            " **];";
	 19: "give~ o testear ~has~ o ~hasnt~ sobre un no-atributo en
	    el objeto ",(name) obj," **];";
         21: print ".&"; 22: print ".#"; 23: print ".";
	}
	"~ sobre ", (name) obj, " **];"; }
    .RErr; if (obj==0 || obj->0>=$70 && obj->0<=$7F) {
	if (obj && obj in Class) print "class ";
	if (obj) print (object) obj;else print "nada";print" ";}
    print "(objeto n�mero ", obj, ") ";
    if (id<0) print "no es una clase ", (name) -id;
    else
    {   print " no tiene la propiedad ", (property) id;
	p = #identifiers_table;
	size = INDIV_PROP_START + p-->3;
	if (id<0 || id>=size)
	    print " (ni la tiene ning�n otro objeto)";
    }
    print " para ", (string) crime, " **];^";
]; 

! Z__Region retorna el tipo de un objeto. Es preferible usar MetaTipo.
! No se proporciona aqui Z__Region pues ser�a id�ntica a la que el
! compilador proporciona en su veneer
![ Z__Region
!    addr tb endmem;
!    if (addr<36) rfalse; ! Direcciones bajas no valen. Retornar 0
!    @getmemsize endmem;  ! Averiguar tama�o de la memoria
!    if (Unsigned__Compare(addr, endmem) >= 0) rfalse;
!          ! Direcciones demasiado grands no valen, retornar 0
!    ! Ahora miramos el tipo en el primer byte del dato
!    tb=addr->0;
!    if (tb >= $E0) return 3; ! Tipo string
!    if (tb >= $C0) return 2; ! Tipo funcion
!    if (tb >= $70 && tb <= $7F && addr >= (0-->2)) ! Tipo objeto
!	return 1;
!    rfalse; ! En otro caso, retornar 0
!];     

! Comparaci�n de dos n�meros considerados sin signo (por defecto,
! Glulx hace comparaciones con signo, para hacerlas sin signo hay que
! recurrir a trucos
![ Unsigned__Compare
!    x y u v;
!    if (x==y) return 0;
!    if (x<0 && y>=0) return 1;
!    if (x>=0 && y<0) return -1;
!    u = x& $7fffffff; v= y& $7fffffff;
!    if (u>v) return 1;
!    return -1;
!];     

! Implementaci�n del operador metaclass
![ Meta__class
!    obj;
!    switch(Z__Region(obj)) {
!     2: return Routine;
!     3: return String;
!     1: if (obj in Class
!	    || obj == Class or String or Routine or Object)
!	 return Class;
!	return Object;
!    }
!    rfalse;
!];     

! Busqueda de un identificador de propiedad dentro de la tabla de
! propiedades de un objeto (b�sico para implementar el provides, y
! tambi�n para otras rutinas del veneer)
![ CP__Tab
!    obj id otab max res;
!    otab = obj-->4;
!    if (otab == 0) return 0;
!    max = otab-->0;
!    otab = otab+4;
!    @binarysearch id 2 otab 10 max 0 0 res;
!    return res;
!];     

! Implementaci�n de las "pseudo-propiedades" de las clases: create,
! destroy, recreate... 
[ CL__Ms 
    _vararg_count obj id a b x y;
    @copy sp obj;
    @copy sp id;
    _vararg_count = _vararg_count - 2;
    switch (id) {
     create:
	! En realidad, los objetos ya est�n creados y guardados como
	! hijos en la clase. Solo hay que sacarlos al mundo del juego.
	if (children(obj) <= 1) rfalse;
	x = child(obj);
	remove x; ! Esto lo saca 
	if (x provides create) { ! Llamar al create del objeto
	    @copy create sp;
	    @copy x sp;
	    y = _vararg_count + 2;
	    @call CA__Pr y 0;
	}
	return x;
     recreate:
	@copy sp a;
	_vararg_count--;
	if (~~(a ofclass obj)) {
	    RT__Err("recrear", a, -obj);
	    rfalse;
	}
	if (a provides destroy)
	    a.destroy();
	! No lo destruimos en realidad... Solo sobreescribimos sus
	! propiedades con otras que copiamos del hijo de la clase.
	Copy__Primitive(a, child(obj));
	if (a provides create) { ! Y llamamos a su create
	    @copy create sp;
	    @copy a sp;
	    y = _vararg_count + 2;
	    @call CA__Pr y 0;
	}
	rfalse;
     destroy:
	@copy sp a;
	_vararg_count--;
	if (~~(a ofclass obj)) {
	    RT__Err("destruir", a, -obj);
	    rfalse;
	}
	! Llamar a la propiedad destroy del objeto (si la tiene)
	if (a provides destroy)
	    a.destroy();
	Copy__Primitive(a, child(obj));
	! Reiniciarlo otra vez con los valores tomados de un hijo
	move a to obj;
	! Y meterlo otra vez en la clase madre
	rfalse;
     remaining:
	! �Cu�ntos quedan?
	! Siempre guardamos uno para usarlo como molde para las
	! copias, por eso retornamos uno menos
	return children(obj)-1;
     copy:
	! Copiar
	@copy sp a;
	@copy sp b;
	_vararg_count = _vararg_count - 2;
	if (~~(a ofclass obj)) {
	    RT__Err("copiar", a, -obj);
	    rfalse;
	}
	if (~~(b ofclass obj)) {
	    RT__Err("copiar", b, -obj);
	    rfalse;
	}
	Copy__Primitive(a, b);
	rfalse;
    }
];

! Verificaci�n de que move obj1 to obj2 puede hacerse, y hacerlo en
! ese caso
[ RT__ChT 
    obj1 obj2 ix;

    ! No se pueden mover los objetos-metaclase
    if (obj1==0 || Z__Region(obj1)~=1
	|| (obj1 == Class or String or Routine or Object) || obj1 in Class)
	return RT__Err(16, obj1, obj2);

    ! Ni se puede mover nada a ellos
    if (obj2==0 || Z__Region(obj2)~=1
	|| (obj2 == Class or String or Routine or Object) || obj2 in Class)
	return RT__Err(17, obj1, obj2);
    ix = obj2;

    ! Comprobar si ocurrir�a recursi�n infinita... (Esto es, si el
    ! objeto obj2 al que se pretende mover es uno de los descendientes
    ! de obj1
    while (ix ~= 0) {
	if (ix==obj1) return RT__Err(18, obj1, obj2);
	ix = parent(ix);
    }

    ! Pasados los test, vamos a moverlo
!    #ifdef INFIX;
!    if (obj1 has infix__watching
!	|| obj2 has infix__watching || (bits_debug & 15))
!	print "[Moviendo ", (name) obj1, " a ", (name) obj2, "];^";
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 8)
	print "[Moviendo ", (name) obj1, " a ", (name) obj2, "];^";
    #endif;
    ! Esta otra rutina es la que hace el movimiento
    OB__Move(obj1, obj2);
];     

! Verificar si remove obj1 es legal, y ejecutarlo en ese caso
[ RT__ChR 
    obj1;
    ! No se puede "remover" algo que no sea un objeto
    if (obj1==0 || Z__Region(obj1)~=1
	|| (obj1 == Class or String or Routine or Object) || obj1 in Class)
	return RT__Err(15, obj1);
    ! Test pasado, informamos de ello
!    #ifdef INFIX;
!    if (obj1 has infix__watching || (bits_debug & 15))
!	print "[Eliminando ", (name) obj1, "];^";
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 8)
	print "[Eliminando ", (name) obj1, "];^";
    #endif;
    OB__Remove(obj1);
];     

! Verificar si "give obj1 a" es legal. Y si lo es, hacerlo
[ RT__ChG
    obj1 a;
    ! No se puede usar give sobre algo que no sea un objeto
    if (Z__Region(obj1) ~= 1) return RT__Err(14,obj1);
    ! Ni tampoco sobre una clase o metaclase
    if (obj1 in Class || obj1 == Class or String or Routine or Object)
	return RT__Err(14,obj1);
    ! El atributo debe ser un n�mero v�lido
    if (a<0 || a>=NUM_ATTR_BYTES*8) return RT__Err(19,obj1);
    ! Si ya lo tenia, no se lo damos
    if (obj1 has a) return;
!    #ifdef INFIX;
!    if (obj1 has infix__watching || (bits_debug & 15))
!	print "[Dando a ", (name) obj1, " el atributo ", (_atributo) a, "];^";
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 4)
	print "[Dando a ", (name) obj1, " el atributo ", (_atributo_) a, "];^";
    #endif; 
    ! Darle el atributo
    obj1++;
    @astorebit obj1 a 1;
];    

! Verificar si es legal "give obj1 ~a" y ejecutarlo en ese caso
[ RT__ChGt
    obj1 a;
    ! No es legal usar give sobre un no-objeto
    if (Z__Region(obj1) ~= 1) return RT__Err(14,obj1);
    ! Ni sobre una clase o metaclase
    if (obj1 in Class || obj1 == Class or String or Routine or Object)
	return RT__Err(14,obj1);
    ! El n�mero de atributo debe ser legal
    if (a<0 || a>=NUM_ATTR_BYTES*8) return RT__Err(19,obj1);
    ! Si no lo ten�a, no se lo quitamos
    if (obj1 hasnt a) return;
!    #ifdef INFIX;
!    if (obj1 has infix__watching || (bits_debug & 15))
!	print "[Quitando a ",(name) obj1," el atributo ", (_atributo) a, "];^";
!    #ifnot; #ifdef DEBUG;
    #ifdef _DEBUG_H_;
    if (bits_debug & 4)
	print "[Quitando a ",(name) obj1," el atributo ", (_atributo_) a, "];^";
    #endif;
    ! Quitarle el atributo
    obj1++;
    @astorebit obj1 a 0;
];    

!================================================================
! COMPROBACIONES PARA MODO ESTRICTO
!================================================================
! Comprobar si se le puede asignar un valor a una propiedad de un
! objeto, y asignarsela si se puede.
[ RT__ChPS
    obj prop val res;
!    #ifdef INFIX;
!    if (obj has infix__watching || (bits_debug & 15)) RT__TrPS(obj,prop,val);
!    #ifnot; #ifdef DEBUG;

    ! No se puede asignar a algo que no sea objeto, ni algo que sea
    ! clase o metaclase
    if (obj==0 || Z__Region(obj)~=1
	|| (obj == Class or String or Routine or Object) || obj in Class)
	return RT__Err("asignar", obj, prop);
    #ifdef _DEBUG_H_;
    if (bits_debug & 2) RT__TrPS(obj,prop,val);
    #endif;
    ! Hagamos la asignaci�n. Pero WV__Pr testea adem�s si el objeto
    ! tiene la propiedad
    res = WV__Pr(obj, prop, val);
    return res;
];     

! Trazado de asignaciones a propiedades
[ RT__TrPS
    obj prop val;
    print "[Asignando a ",(name) obj,".",(property) prop,
	" el valor ",val,"];^";
];

! Comprobar si se puede leer un byte (->)
[ RT__ChLDB 
    base offset a b val;
    a=base+offset;
    @getmemsize b;
    ! El byte puede leerse siempre que est� dentro del
    ! espacio de direcciones
    if (Unsigned__Compare(a, b) >= 0)
	return RT__Err(24);
    @aloadb base offset val;
    return val; ! Lo escribimos y retornamos el valor escrito en ese caso
];

! Comprobar si se puede leer un word (-->)
[ RT__ChLDW 
    base offset a b val;
    a=base+WORDSIZE*offset;
    @getmemsize b;
    if (Unsigned__Compare(a, b) >= 0)
	return RT__Err(25);
    @aload base offset val;
    return val;
];     


! Comprobar si se puede escribir un byte (->)
[ RT__ChSTB 
    base offset val a b;
    a=base+offset;
    ! Que est� dentro del espacio de direcciones
    @getmemsize b;
    if (Unsigned__Compare(a, b) >= 0) jump ChSTB_Fail;
    ! Que no sea memoria ROM
    @aload 0 2 b;
    if (Unsigned__Compare(a, b) < 0) jump ChSTB_Fail;
    @astoreb base offset val;
    return;
    .ChSTB_Fail;
    return RT__Err(26);
];     
! Comprobar si se puede escribir un word (-->)
[ RT__ChSTW
    base offset val a b;
    a=base+WORDSIZE*offset;
    ! Comprobar que est� dentro del espacio de direcciones
    @getmemsize b;
    if (Unsigned__Compare(a, b) >= 0) jump ChSTW_Fail;
    ! Y que no sea memoria ROM
    @aload 0 2 b;
    if (Unsigned__Compare(a, b) < 0) jump ChSTW_Fail;
    @astore base offset val;
    return;
    .ChSTW_Fail;
    return RT__Err(27);
];

! Comprobar si se puede print (char), o sea si el car�cter es
! imprimible, o sea, que est� dentro del rango de caracteres
! imprimibles del ISO-Latin-1
[ RT__ChPrintC 
    c;
    if (c<10 || (c>10 && c<32) || (c>126 && c<160) || c>255)
	return RT__Err(33,c);
    ! Si lo es, lo mostramos
    @streamchar c;
];     

! Comprobar si se puede print (address), y hacerlo si se puede
[ RT__ChPrintA 
    addr endmem;
    if (addr<36)
	return RT__Err(34);
    @getmemsize endmem;
    if (Unsigned__Compare(addr, endmem) >= 0)
	return RT__Err(34);
    if (addr->0 ~= $60)
	return RT__Err(34);
    Print__Addr(addr);
];     

! Comprobar si se puede print (string) y hacerlo si se puede
[ RT__ChPrintS 
    str;
    if (Z__Region(str) ~= 3)
	return RT__Err(35);
    @streamstr str;
];     

! Comprobar si se puede print (object) y hacerlo si se puede
[ RT__ChPrintO 
    obj;
    if (Z__Region(obj) ~= 1)
	return RT__Err(36);
    @aload obj 3 sp; @streamstr sp;
];     

! Implementa move obj1 to dest
!
! Los test para comprobar si el movimiento es legal ya han sido hechos
! en RT__ChT.
[ OB__Move
    obj dest par chi sib;
    ! Averiguar mi padre actual
    par = obj-->((NUM_ATTR_BYTES+1)/4 +3);
    if (par ~= 0) {
	! Averiguar el child del padre
	chi = par-->((NUM_ATTR_BYTES+1)/4+5);
	if (chi == obj) {
	    ! si soy yo mismo, es que soy el primero de la familia.
	    ! Antes de irme, convierto en primero de la familia a mi
	    ! hermano.
	    par-->((NUM_ATTR_BYTES+1)/4+5) =
		obj-->((NUM_ATTR_BYTES+1)/4+4);
	}
	else {
    	    ! Si no soy el primero de la familia, debo encontrar al
    	    ! hermano que apunta a m�, y hacer que apunte a mi pr�ximo
    	    ! hermano.
	    while (1) {
		sib = chi-->((NUM_ATTR_BYTES+1)/4+4);
		if (sib == obj)
		    break;
		chi = sib;
	    }
	    ! Asignar a mi hermano por la izquierda el puntero a mi
	    ! hermano por la derecha
	    chi-->((NUM_ATTR_BYTES+1)/4+4) = obj-->((NUM_ATTR_BYTES+1)/4+4);
	}
    }
    ! Ya he dicho adios a mi antigua familia, entremos en la nueva
    ! Primero, se�alar qui�n es mi nuevo hermano (ser� el primog�nito
    ! de mi nuevo padre)
    obj-->((NUM_ATTR_BYTES+1)/4+4) = dest-->((NUM_ATTR_BYTES+1)/4+5);
    ! Ahora, quien es mi nuevo padre
    obj-->((NUM_ATTR_BYTES+1)/4 + 3) = dest;
    ! Y finalmente, decirle a mi nuevo padre que soy su nuevo primog�nito
    dest-->((NUM_ATTR_BYTES+1)/4 + 5) = obj;
    rfalse;
];     

! Implementaci�n de remove. Las comprobaciones de que la operaci�n es
! v�lida ya han sido hechas en RT__ChR
[ OB__Remove
    obj par chi sib;

    ! �Quien es mi padre actual?
    par = obj-->((NUM_ATTR_BYTES+1)/4 + 3);
    if (par == 0)
	rfalse;   ! No tengo padre, pues ya no hay que "removerme"
    ! �Qui�n es el primog�nito de mi padre?
    chi = par-->((NUM_ATTR_BYTES+1)/4 + 5);
    if (chi == obj) {
	! Si soy yo, antes de irme de esta familia dejo como
	! primog�nito a mi hermano por la derecha.
	par-->((NUM_ATTR_BYTES+1)/4 + 5) = obj-->((NUM_ATTR_BYTES+1)/4
						  + 4);
    }
    else {
	! Si no soy yo, busco a mi hermano por la izquierda
	while (1) {
	    sib = chi-->((NUM_ATTR_BYTES+1)/4 + 4);
	    if (sib == obj)
		break;
	    chi = sib;
	}
	! Y le digo a mi hermano de la izquierda que su hermano es mi
	! hermano por la derecha. De este modo abandono esta familia
	chi-->((NUM_ATTR_BYTES+1)/4 + 4) = obj-->((NUM_ATTR_BYTES+1)/4
						  + 4);
    }
    ! Ahora, no tengo padre ni hermanos
    obj-->((NUM_ATTR_BYTES+1)/4+3) = 0;
    obj-->((NUM_ATTR_BYTES+1)/4+4) = 0;
    rfalse;
];     

! Implementaci�n de print (address)
[ Print__Addr
    addr ix ch;
    ! Verifiquemos que realmente es una palabra de diccionario
    if (addr->0 ~= $60) {
	print "(", addr, ": no es una palabra del diccionario)";
	return;
    }
    ! Imprimirla es f�cil. Son cadenas de caracteres terminados en 0,
    ! con una longitud m�xima de DICT_WORD_SIZE
    for (ix=1 : ix <= DICT_WORD_SIZE : ix++) {
	ch = addr->ix;
	if (ch == 0)
	    return;
	print (char) ch;
    }
];

! Implementa el comando glk(). Simplemente pasa sus par�metros al
! opcode @glk y retorna el resultado de esa llamada
[ Glk__Wrap 
    _vararg_count callid retval;
    @copy sp callid;
    _vararg_count = _vararg_count - 1;
    @glk callid _vararg_count retval;
    return retval;
];


! Implementaci�n de cadenas din�micas. Simplemente asigna el valor que
! se le pasa en una posici�n de la tabla de strings din�micos.
! El valor puede tratarse de otro string o de una rutina.
[ Dynam__String
    num val;
    if (num < 0 || num >= #dynam_string_table-->0)
	return RT__Err(40, num);
    (#dynam_string_table)-->(num+1) = val;
];     

!================================================================
! Las rutinas siguientes ya no forman parte del veneer, pero en mi
! opini�n deber�a haber un operador inform para ellas, pues se trata
! en el fondo de rutinas con cometidos similares a todas las
! anteriores

! Imprime el nombre de un atributo que recibe como par�metro.
[ _atributo_ a str;
    if (a<0 || a>=NUM_ATTR_BYTES*8) print "<atributo no v�lido ", a, ">";
    else {
    	str = #identifiers_table-->4;
    	str = str-->a;
    	if (str) print (string) str;
    	else print "<atributo sin nombre ", a, ">";
    }
];

! ________________________________________________________________
! Rutinas para el manejo del tipado en tiempo de ejecuci�n
Constant TIPO_STRING 1;
Constant TIPO_DICCIONARIO 2;
Constant TIPO_RUTINA 3;
Constant TIPO_OBJETO 4;
Constant TIPO_CLASE 5;
Constant TIPO_ARRAY 6;
Constant TIPO_RUTINA_STACK 7;
Constant TIPO_OTRO 8;

! Rutina para averiguar en tiempo de ejecuc��n el tipo de un dato
[ MetaTipo x;
    if ((x->0 == 0) && (metaclass(x) == 0)) return TIPO_ARRAY;
    switch(x->0)
    {
     $60: return TIPO_DICCIONARIO;
     $C0: return TIPO_RUTINA_STACK;
     $C1: return TIPO_RUTINA;
     $E1: return TIPO_STRING;
     $70:
	if (metaclass(x)==Object) return TIPO_OBJETO;
     	else return TIPO_CLASE;
     default: return TIPO_OTRO;
    }
];

! ________________________________________________________________
! Rutinas para b�squeda de elementos en arrays o propiedades
!

! Rutina para buscar un elemento en un array de bytes
!  recibe la direcci�n de comienzo de un array y opcionalmente su
!  longitud (si esta se omite, se entiende que es el primer elemento
!  [tama�o byte] del array, para soportar el tipo Array String)
!
! retorna false si el elemento no est�, o bien el �ndice+1 donde se ha
! encontrado.
[ EstaEnArrayBytes x arr l r;
    if (l==0) {
	l=arr->0;
	arr++;
    }

!    * L1: Key
!    * L2: KeySize
!    * L3: Start
!    * L4: StructSize
!    * L5: NumStructs
!    * L6: KeyOffset
!    * L7: Options
!    * S1: Result

    @linearsearch x 1 arr 1 l 0 4 r;
    if (r==-1) rfalse;
    return (r +1);
];

! Rutina para buscar un elemento en un array de bytes
!  recibe la direcci�n de comienzo de un array y opcionalmente su
!  longitud (si esta se omite, se entiende que es el primer elemento
!  [tama�o word] del array, para soportar el tipo Array Table)
!
! retorna false si el elemento no est�, o bien el �ndice+1 donde se ha
! encontrado.
[ EstaEnArrayWords x arr l r;
    if (l==0) 
    {
	l=arr-->0;
	arr=arr+4;
    }
    @linearsearch x 4 arr 4 l 0 4 r;
    if (r==-1) rfalse;
    return (r +1);
];

! Retorna el n�mero de elementos que un objeto tiene almacenados en
! una de sus propiedades
[ ElementosEnPropiedad obj p;
    return(obj.#p/4);
];

! Retorna el valor del elemento i�simo de la propiedad p del objeto
! obj
[ ValorDePropiedad obj p i;
    if (i==0) return obj.p;
    else return(obj.&p-->i);
];

! Retorna el valor del elemento i�simo de la propiedad p del objeto
! obj, entendiendo que los elementos de esa propiedad son tipo byte
[ ValorByteDePropiedad obj p i;
    return (obj.&p->i);
];

! Busca el dato "valor" en la lista de elementos de la propiedad p del
! objeto obj
[ BuscarEnPropiedad valor obj p mem l i;
    if (~~(obj provides p)) rfalse;
    mem=obj.&p;
    l=obj.#p;
    @linearsearch valor 4 mem 4 l 0 4 i;
    if (i==-1) rfalse;
    return (i+1);
];


! Rutinas genericas para manejo del arbol de objetos Inform
! ================================================================
! Todos los objetos que conponen un juego est�n almacenados en glulx
! en forma de lista enlazada, que es la que objectloop va recorriendo
!
! Cada elemento de la lista, contiene un puntero hacia el siguiente. Y
! adem�s, contiene punteros hacia su padre, hermano e hijo, lo que
! permite implementar el �rbol de objetos.
!
! La estructura interna de un objeto es:
!
!   ->  -->  Contenido
!  ------------------
!    0       70h (identificador del tipo "objeto")
!  1 a X     Atributos
!       Y+0   Puntero al siguiente en la lista enlazada
!       Y+1   Puntero al string del nombre interno
!       Y+2   Puntero a la tabla de propiedades
!       Y+3   Puntero al padre
!       Y+4   Puntero al hermano
!       Y+5   Puntero al hijo
!
!  X es igual a la constante MAX_ATTR_BYTES (7 por defecto)
!  Y es igual a (MAX_ATTR_BYTES+1)/4  (2 por defecto)
!
!  En general, el puntero en Y+0 no cambia de valor, pero se puede
!  cambiar para que objectloop recorra la lista en otro orden. Los
!  punteros en Y+1, Y+2 e Y+3 son actualizados por cada instrucci�n
!  move o remove, pero a continuaci�n se dan rutinas que lo actualizan
!  por si mismas para lograr algunos efectos especiales, ya que move
!  siempre inserta el objeto como "primer hijo", pero gracias a estas
!  rutinas podremos insertarlo en cualquier lugar de su lista de
!  hijos.
!
[ MoverComoHijoPrimero obj padre;
    move obj to padre;
];

[ MoverJuntoHermano obj hermano aux;
    ! Convierte obj en el hermano de "hermano". Por tanto queda
    ! colocado a su derecha en la lista de hermanos.
    
    ! No se puede hacer uno hermano de un objeto sin padre
    if (parent(hermano)==0)
	"^[** Error de programaci�n: se intent� MoverJuntoHermano(",
	    (name) obj, ", ", (name) hermano, ") pero ", 
    	    (name) hermano, " no tiene padre. **]";
    
    ! Comprobar si ocurrir�a recursi�n infinita... (Esto es, si el
    ! objeto "hermano" del que quiero hacerme hermano es uno de los
    ! descendientes de "obj"
    aux=hermano;
    while (aux ~= 0) {
	if (aux==obj)
	    "^[** Error de programaci�n: se intent� 
	    MoverJuntoHermano(", (name) obj, ", ", (name) hermano, ") lo
	    que causar�a un error de recursi�n infinita en ", (name)
		obj, " **]";
	aux = parent(aux);
    }
    #ifdef _DEBUG_H_;
    ! Traza
    if (bits_debug & 8) 
    {
	print "[Moviendo ", (name) obj, " para que sea hermano de ",
	    (name) hermano, "]^";
	aux=bits_debug;
	bits_debug = 0;
	remove obj;
	bits_debug = aux;
    }
    else remove obj;
    #ifnot;
    remove obj;
    #endif;

    ! Bajo nivel. Modificaci�n de los punteros del objeto
    ! +3 = padre
    ! +4 = hermano
    ! +5 = hijo
    aux=hermano-->((NUM_ATTR_BYTES+1)/4 + 4);
    hermano-->((NUM_ATTR_BYTES+1)/4 +4)=obj;
    obj-->((NUM_ATTR_BYTES+1)/4 +4)=aux;
    aux=parent(hermano);
    obj-->((NUM_ATTR_BYTES+1)/4 +3)=aux;
];

[ MoverComoHijoUltimo obj padre hijos i j;
    hijos=children(padre);
    if (hijos==0) { move obj to padre;	return;    }
    objectloop(i in padre) {
	j++;
	if (j==hijos) { MoverJuntoHermano(obj, i); return; }
    }
];

[ MoverComoHijoN obj padre n
    i j hijos;
    if (n==0) move obj to padre;
    hijos=children(padre);
    if (n>=hijos) MoverComoHijoUltimo(obj, padre);
    objectloop (i in padre)  {
	j++;
	if (j==n) { MoverJuntoHermano(obj, i);  return;	}
    }
];

! Inserta un elemento antes o despues de otro dado. Retorna el nuevo padre.
[ InsertarDespuesDe obj1 obj2 aux;
    if (parent(obj2)==0)
	"^[** Error de programaci�n: se intent� InsertarDespuesDe ",
	    (name) obj2, " pero �ste objeto no tiene padre **]";
    aux=aux;
    #ifdef _DEBUG_H_;
    if (bits_debug & 8) 
    {
	aux = bits_debug;
	bits_debug=0;
    	MoverJuntoHermano(obj1, obj2);
	print "[Insertando ", (name) obj1, " despu�s de ", (name)
	    obj2, "]^";
	bits_debug=aux;
	return (parent(obj1));
    }
    #endif;
    MoverJuntoHermano(obj1, obj2);
    return (parent(obj1));
];

[ InsertarAntesDe obj1 obj2 p chi sib aux;
    p=parent(obj2);
    if (p==0)
	"^[** Error de programaci�n: se intent� InsertarAntesDe ",
	    (name) obj2, " pero �ste objeto no tiene padre **]";
    aux=aux;
    chi=child(p);
    #ifdef _DEBUG_H_;
    if (bits_debug&8) print "[Insertando ", (name) obj1, " antes de
	", (name) obj2, "]^";
    aux = bits_debug;
    bits_debug=0;
    #endif;
    if (chi == obj2) {
	move obj1 to p;
    }
    while (1) {
	sib = chi-->((NUM_ATTR_BYTES+1)/4+4);
	if (sib == obj2)
	    break;
	chi = sib;
    }
    MoverJuntoHermano(obj1, chi);
    #ifdef _DEBUG_H_;
    bits_debug=aux;
    #endif;
    return p;
];
#endif;
