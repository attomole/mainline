! io.h
! ===============================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! INPUT/OUTPUT b�sico
!
! Rutinas de entrada para Glulx
!
#ifndef _IO_H_;
Message "..Incluyendo rutinas de Entrada/Salida";
System_file;
Constant _IO_H_;

! M�ximo de caracteres que se pueden leer con las rutinas de Input
Constant TAMANYO_BUFFER_INPUT 120;

! Arrays para recibir par�metros y respuestas de Glk
Array gg_event --> 4;
Array gg_arguments --> 8;

! BN_Input: rutina de entrada de bajo nivel
!           recibe como par�metro de qu� ventana debe obtener el input
!           y un array donde dejar la cadena de letras leida.
[ BN_Input  ventana resultado done x;
    done = false;
    glk($00D0, ventana, resultado+WORDSIZE,
      	TAMANYO_BUFFER_INPUT-WORDSIZE, 0); ! glk_request_line_event
    while (~~done) {
      	glk($00C0, gg_event); ! glk_select
      	switch (gg_event-->0) {
     	 3: ! evtype_LineInput
	    ! [1] = ventana en que ha ocurrido el evento
	    ! [2] = numero de letras leidas
            if (gg_event-->1 == ventana) {
          	resultado-->0 = gg_event-->2;
            	done = true;
            }
    	}
	! Pasar el evento al gancho de usuario
	x = EjecutarGancho(EventoGlk, gg_event, 0, resultado);
	if (x==2) {
	    glk(209, ventana, 0); ! glk_cancel_line_event
	    done = true;
	}
	else if (x==-1) done = false;
    }
    ! Otro gancho justo antes de finalizar el input, con el resultado obtenido
    EjecutarGancho(PostInput, ventana, resultado);
];

! BN_Output: Rutina de salida de bajo nivel
!     Emite texto por la ventana que recibe como primer par�metro. El
!     texto puede ser un string, un array de letras o una palabra de 
!     diccionario. La propia rutina detecta el caso.
[ BN_Output _vararg_count ventana cosa  stream_anterior;
    if (_vararg_count == 0)
        return;
    @copy sp ventana;
    _vararg_count--;
    if (ventana==0) return;
    
    @copy sp cosa;
    _vararg_count--;
    if (cosa == 0)
        return;

    stream_anterior=glk($48); ! glk_stream_get_current
    glk($2F, ventana);   ! glk_set_window (cambia el stream de salida)
    @copy cosa sp;
    _vararg_count++;
    @call Output _vararg_count 0;
    glk($47, stream_anterior); ! glk_stream_get_current
];

! La rutina que verdaderamente imprime, tras determinar el tipo de
! par�metro recibido, en el stream actual
[ Output _vararg_count cosa i;
    if (_vararg_count == 0)
        return;
    @copy sp cosa;
    _vararg_count--;
    if (cosa == 0)
        return;
    switch(MetaTipo(cosa))
    {
     TIPO_STRING:
	print (string) cosa;
     TIPO_ARRAY:
	for (i=WORDSIZE:i<cosa-->0 + WORDSIZE:i++)
	    print (char) cosa->i;
     TIPO_DICCIONARIO:
	print (address) cosa;
     TIPO_OTRO:
	break;
     TIPO_RUTINA:
	@call cosa _vararg_count 0;
     TIPO_OBJETO, TIPO_CLASE:
	if (_vararg_count == 0) {
	    print (name) cosa;
	}
	else {
	    ! En este caso, volvemos a dejar el objeto en la pila y
	    ! llamamos a la rutina del veneer CA__Pr que es la que
	    ! evalua propiedades de objetos
	    @copy cosa sp ;
	    _vararg_count++;
	    @call CA__Pr _vararg_count 0;
	}
    }
];    


! Limpiar ventana
!
[ LimpiarVentana w;
    glk(42, w);
];

! BN_Tecla
!
!  Lectura de un evento de tecla de bajo nivel. Espera hasta que se
!  haya pulsado una tecla u ocurra otro tipo de evento. Llama a un
!  gancho ante cualquier tipo de evento. Si el gancho retorna 2, la
!  funci�n retornar� el resultado que el gancho le diga (en lugar de
!  la tecla pulsada). Si el gancho retorna -1, no se sale de esta
!  funci�n y se seguir� esperando por un car�cter m�s.
!
[ BN_Tecla ventana listo res x;
    if (ventana == 0)
      	return;
    listo = false;
    glk($00D2, ventana); ! glk_request_char_event
    while (~~listo) {
      	glk($00C0, gg_event); ! glk_select
      	switch (gg_event-->0) {
      	 2: ! evtype_CharInput
            if (gg_event-->1 == ventana) {
          	res = gg_event-->2;
          	listo = true;
            }
    	}
      	! LLamada al gancho del usuario
    	x = EjecutarGancho(EventoGlk, gg_event, 1, gg_arguments);
    	if (x == 2) {
      	    res = gg_arguments-->0;
      	    listo = true;
    	}
    	else if (x == -1) {
      	    listo = false;
    	}
    }
    return res;
];

! Imprime la x como si fuera un numero. Pensado para usar en
! conjuncion con ImprimirEnArray, en la forma siguiente:
!  ImrpimirEnArray(datonumerico, 6);
[ DatoNumerico x;
    print x;
];

[ ImprimirEnMemoria  _vararg_count arr arrlen str oldstr len;

   @copy sp arr;
   @copy sp arrlen;
   _vararg_count = _vararg_count - 2;

   oldstr = glk($0048); ! stream_get_current
   str = glk($0043, arr, arrlen, 1, 0); ! stream_open_memory
   if (str == 0)
       return 0;
   glk($0047, str); ! stream_set_current
   @call Output  _vararg_count 0;
   glk($0047, oldstr); ! stream_set_current
   @copy $ffffffff sp;
   @copy str sp;
   @glk $0044 2 0; ! stream_close
   @copy sp len;
   @copy sp 0;
   return len;
];

! Ganchos para el usuario
_Gancho_ EventoGlk;
! rutina(gg_event, cuando, valor_retornado)
!  * en gg_evant-->0 tiene el tipo de evento y su valor en otros campos
!    de gg_event
!  * en "cuando" tiene un 0 si el evento se produjo mientras se
!    esperaba una l�nea de texto del teclado, y un 1 si fue mientras se
!    esperaba un evento de caracter
!  * valor_retornado es una direcci�n donde dejar el resultado. En
!    caso de que "cuando==0", ahi se puede dejar un nuevo buffer de
!    texto como si el jugador lo hubiese tecleado, y retornar 2. En
!    caso de "cuando==1" puede dejarse una letra, como si el jugador
!    hubiese pulsado ese caracter, y retornar 2.
!    En cualquier caso se puede retornar -1 y entonces se ignora esa
!    linea o caracter y se espera otro.

_Gancho_ PostInput;
! rutina(ventana, resultado)
!   * ventana es la ventana de la que se acaba de obtener el input
!   * resultado es el buffer de caracteres, aun sin tokenizar. El
!     gancho puede reemplazarlo completamente por otra secuencia de
!     letras y ser� como si el jugador hubiese escrito eso en su lugar.


#endif;
