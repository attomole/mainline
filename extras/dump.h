! dump.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
! ________________________________________________________________
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
! 
! Rutinas para volcar informaci�n interna sobre datos de la m�quina Glulx
System_file;
! Rutina para imprimir un d�gito hexadecimal
[ hexdig x;
    if ((x<0)||(x>15)) return;
    switch(x) 
    {
     10: print "A";
     11: print "B";
     12: print "C";
     13: print "D";
     14: print "E";
     15: print "F";
     default: print x;
    }
];

! Rutina para imprimir un byte en hexadecimal
[ hex x ;
    print (hexdig) x/16;
    print (hexdig) x%16;
];

[ MostrarHijos o i;
    objectloop(i in o) {
	BN_Output(gg_main, i);
	print "^";
    }
];

[ MostrarArbol o __tab i j;
    if (o==0) return;
        if (__tab==0) {
    	glk($86,2); ! estilo preformateado
	print (name) o, "^";
    	glk($86,0);  ! estilo normal
    }
    objectloop(i in o) 
    {
    	glk($86,2); ! estilo preformateado
    	for (j=0:j<=__tab:j++) print "  ";
	print (name) i, "^";
   	glk($86,0);  ! estilo normal
	if (children(i)) {
	    __tab++;
	    MostrarArbol(i,__tab);
	    __tab--;
	}
    }
];

[ MostrarObjeto o;
    print "^";
    Output(o);
    print "^ Padre --->"; Output(o-->5);
    print "^ Herm ---->"; Output(o-->6);
    print "^ Hijo ---->"; Output(o-->7);
    print "^ NEXT ---->"; Output(o-->2);
    print "^ nombre -->"; Output(o-->3);
    print "^ T.Prop -->"; print o-->4;
];

[ MostrarTipo x;
    switch(MetaTipo(x))
    {
     TIPO_STRING: print "[STRING]";
     TIPO_ARRAY:  print "[ARRAY]";
     TIPO_RUTINA: print "[RUTINA]";
     TIPO_DICCIONARIO: print "[DICCIONARIO]";
     TIPO_OBJETO: print "[OBJETO]";
     TIPO_CLASE:  print "[CLASE]";
     TIPO_OTRO:   print "[OTRO]";
     TIPO_RUTINA_STACK: print "[RUTINA-S]";
    }
];

[ MostrarPropiedades o
    direcc i aux p;
    print "^";
    print "Propiedades de ", (name) o, ". ";
    direcc=o-->4;
    print direcc-->0, " propiedades en total.^";
    for (i=0:i<direcc-->0:i++) 
    {
	print "Propiedad ", i, ":^";
	aux=direcc+4+i*10;
	p=256*aux->0 + aux->1;
	print "       id:", (hex) aux->0, (hex) aux->1, " (",
	    (property) p, ")^";
	print " longitud:", 256*aux->2 + aux->3, "^";
	print "   direcc:", aux-->1, "^";
	MostrarTipo(o.p);
	print "^";
	print "    flags:", (hex) aux->8, (hex) aux->9;
 	if (aux->9 == 0) print "(p�blica)^";
	else print "(privada)^";
    }
];

[ MostrarOrdenacionGancho G pref i;
    if (~~(G ofclass _Gancho_))
	"MostrarOrdenacionGancho: el objeto ", (name) G, " no es un
	gancho.^";
    if (pref==0) print "Orden de ejecuci�n de las rutinas en ", (name) G,
	":^";
    objectloop(i in G)
    {
	if (pref) Output(pref);
	print (name) i;
	if (pref) print "^";
	else
 	    if (sibling(i)) print ", ";
    }
    if (~~pref) print ".^";
];    

[ MostrarTodosLosGanchos i;
    objectloop(i ofclass _Gancho_) 
	if (~~parent(i)) 
	{
	    print (name) i;
	    if (children(i)==0) print " (gancho vac�o)^";
	    else {
		print " (contiene ", children(i), " rutinas)^";
		MostrarOrdenacionGancho(i, "-->");
	    }
	}
];
