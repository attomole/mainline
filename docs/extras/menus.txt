menus.h
================================================================
v0.6.1
Autor:  Zak McKracken
Modificaciones:  Enrique D. Bosch "Presi"
________________________________________________________________
Sinopsis 

   Constantes:
     Ninguna.

   Rutinas:
     AsciiAEntero
     LanzarMenu

   Ganchos:
      Define: Ninguno.
      Utiliza: EventoGlk 
                 en el que instala MenuEventoGlk

   Variables globales:
      Menu_en_ejecucion (de uso interno)
      Array array_input (si no estaba ya declarado)

   Clases:
      _menu_
      Metodos:
        ascii_a_entero
        lanzar_menu
      Propiedades:
        mens_eleg
        mens_fin
        mens_noval
        mens_cont
        no_dos_puntos

   Objetos predefinidos:
      obj_menu

   Incluye: 
      estilos.h

________________________________________________________________
Funcionalidad

  Permite al programador crear men�s usando una estructura de objetos,
  y se ocupa de mostrar esos men�s en pantalla y permitir al jugador
  navegar por ellos, ya sea seleccionando la opcion de men� por su
  n�mero, o pinchando sobre ella con el rat�n.

________________________________________________________________
Rutinas

  La rutina b�sica que proporciona es:

  * LanzarMenu(menu_raiz, borrando, nocero, nocont)

      menu_raiz debe ser un objeto que contiene otros subobjetos (que
      son las opciones del men�). Esta rutina simplemente imprime con
      estilo subcabecera el nombre del objeto "menu_raiz", y
      seguidamente, uno en cada l�nea y precedido por un n�mero, el
      nombre de cada uno de los objetos que hay dentro dem menu_raiz.
      A la vez, define hiperenlaces para cada uno de los nombres de
      los hijos.

      Finalmente llama a BN_Input para esperar a que el usuario
      seleccione una opci�n de men� (escribiendo su numero). Si el
      usuario pincha con el rat�n sobre una de ellas, se producir� un
      evento de hiperenlace, que ser� manejado por el gancho EventoGlk
      que veremos luego, y lo convertir� en un n�mero de opci�n. Sea
      como sea, BN_Input finalmente retornar� una cadena de texto que
      es la selecci�n realizada por el jugador, y que se espera sea un
      n�mero de opci�n. Mediante una llamada a AsciiAEntero, se
      convierte esta cadena en un valor num�rico, con lo que es
      seleccionada una de las opciones (si la selecci�n es inv�lida,
      se presenta de nuevo el men� para que se elija otra vez una
      opci�n v�lida).

      Se examina entonces el objeto seleccionado, y si �ste tiene
      hijos, se llama de nuevo a LanzarMenu, esta vez con este objeto
      como menu_raiz. Si no tiene hijos, pero proporciona una
      propiedad llamada "rutina", �sta es enviada a Output, con lo que
      ser� impresa si se trata de una cadena, o ser� ejecutada si
      realmente es una rutina. Finalizada la ejecuci�n de esta rutina,
      se espera por una pulsaci�n de tecla, si "nocont" es true
      finaliza la ejecuci�n del men�, en otro caso (por defecto) se 
      vuelve a mostrar el menu_raiz.

      Si "nocero" es true no s� mostrar� la opci�n 0 y el men� acabar�
      su ejecuci�n una vez acabado la ejecuci�n de la opci�n
      seleccionada, "nocero" a true implica tambi�n "nocont" a true 
      aunque no se especifique.

      Si el usuario decide abandonar (escribiendo la opci�n 0), la
      rutina EjecutarMenu retornar� (el efecto es que se vuelve al
      men� padre si se estaba en un submen�, o se retorna por completo
      si no).

  * AsciiAEntero(buffer, longitud)
      Recibe un array de letras (se espera que buffer->0 contenga ya
      la primera letra, es decir, no la longitud del array) y un
      par�metro "longitud" que le indica cu�ntos caracteres
      interpretar.

      Retornar� el valor num�rico que hay representado en la secuencia
      de letras del buffer. Por ejemplo, si el buffer tiene la
      secuencia de caracteres "123", se retornar� el valor entero 123.
      Si el buffer contiene alg�n car�cter no num�rico, se retorna -1.

  Nuevo a partir de la versi�n 0.6:

  Alternativamente pueden usarse los m�todos de la clase "_menu_", con
  el objeto predefinido "obj_menu", o con cualquier otro que se
  instancie:

  obj_menu.lanzar_menu()
  obj_menu.ascii_a_entero()

  Con funcionalidad exactamente igual a las funciones explicadas m�s
  arriba.

  A la clase "_menu_" se le han a�adido propiedades para que el usuario
  pueda cambiar los textos que mostrar� el men�:

  obj_menu.mens_eleg
    mensaje antes de elegir opci�n
    por defecto: "Elige opcion"
  obj_menu.mens_fin
    texto de la opcion 0 del men�
    por defecto: "(0 finalizar)"
  obj_menu.mens_noval
    mensaje de error cuando se da una opci�n no v�lida
    por defecto: "Opci�n no v�lida"
  obj_menu.mens_cont
    mensaje para continuar
    por defecto: "[Continuar]"
  obj_menu.no_dos_puntos
    si es true no imprime ":" despu�s del texto
    obj_menu.mens_fin
    por defecto: false

________________________________________________________________
Ganchos

  Este m�dulo instala una rutina en el gancho EventoGlk.

  A trav�s de ella, se le notifica cu�ndo ocurre cualquier evento. La
  rutina se interesa s�lo por los eventos de tipo hyperlink (que
  ocurren cuando el usuario pulsa con el rat�n sobre un texto marcado
  como hiperenlace). Cuando esto ocurre, la rutina sustituye el buffer
  de texto en el que el usuario estaba escribiendo por un buffer
  nuevo, que contiene en forma de cadena el n�mero de la selecci�n
  sobre la que ha pinchado con el rat�n. Despu�s retorna 2, lo que
  tiene el efecto (v�ase io.h) de abortar la entrada de BN_Input y
  hacer que �ste retorne lo que hay en su buffer (que es lo que el
  gancho acaba de modificar).

  El efecto es que cuando el jugador pulsa sobre una opci�n de menu,
  es como si hubiera escrito su n�mero y pulsado el retorno de carro.


