! filtros.h
! ================================================================
! v0.6.1
! Autor: (C) Zak McKracken
!
!    This library is free software; you can redistribute it and/or
!    modify it under the terms of the GNU Lesser General Public
!    License as published by the Free Software Foundation; either
!    version 2.1 of the License, or (at your option) any later version.
!
!    This library is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
!    Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public
!    License along with this library; if not, write to the Free Software
!    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
!
! ________________________________________________________________
! Filtrado de la salida a trav�s de ganchos.
#ifndef _FILTROS_H_;
Message "[Incluyendo modulo extra para soporte del filtrado de la salida]";
System_File;
Constant _FILTROS_H_;

_Gancho_ InicializarFiltroDeSalida InicializarModulos
    with rutina [;
	     ! Al cargar este modulo, se activa el filtro (por
	     ! defecto, el filtro que se proporciona no filtra nada)
	     ActivarFiltroSalida();
    	 ];

! Declara un nuevo gancho en el que meter los filtros, y un filtro por
! defecto que no filtra nada.
_Gancho_ FiltroDeSalida;

_Gancho_  FiltroDefault FiltroDeSalida
    with rutina [ c;
	     glk(128, c);  ! Emite el caracter recibido
	 ],
    posicion GANCHO_EN_ULTIMA_POSICION;  ! Para que sea el filtro por defecto.

[ IosysFiltroGenerico c;
    ! Al usar el mecanismo de ganchos, los filtros definidos en otros
    ! modulos se van aplicando en secuencia sobre el caracter c. Se
    ! entiende que los otros modulos deber�an ser bloqueantes del tipo
    ! BLOQUEA_SI_FALSE, y de este modo, el primer filtro que haya
    ! filtrado la letra y no desee que sea impresa, retornar�a
    ! true. Si todos van retornando false se acabar� en el filtro por
    ! defecto que emite el car�cter.
    EjecutarGancho(FiltroDeSalida, c);
];

[ ActivarFiltroSalida;
    @setiosys 1 IosysFiltroGenerico;
];

[ DesactivarFiltroSalida;
    @setiosys 2 0;
];
#endif;    
